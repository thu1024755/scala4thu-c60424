package cc

/**
  * Created by mark on 16/04/2017.
  */
case class Fraction(n:Int, d:Int) {
  require(d!=0,"Error!!")

  //計算最大公因數(greatest common divisor)
  def calGCD(a:Int, b:Int):Int ={
    if(b==0 ) a else calGCD (b,a%b)
  }

  def reduce()={
    val gcd=calGCD(n,d)
    Fraction(n/gcd,d/gcd)
  }
  def +(that:Fraction): Fraction ={
    Fraction(this.n*that.d+this.d*that.n,this.d*that.d).reduce()
  }

  def equal(that:Fraction): Boolean ={
   if (that.d/this.d.toDouble==that.n/this.n.toDouble) true
    else false
  }


  override def toString: String = n+"/"+d


}

object Fraction{
  def apply(n: Int): Fraction = new Fraction(n, 1)
}


